# [NEWS TROOP Discord Notification Bot v1.3](https://sites.google.com/view/newstroop/home)
<img style="-webkit-user-select: none;margin: auto;" src="https://cdn.discordapp.com/attachments/637125522886885442/637165414539067412/NEWSTROOPTEST-2.gif">

## What is NEWS TROOP
[We](https://sites.google.com/view/newstroop/home) are a news outlet like no other.

[Visit here](https://sites.google.com/view/newstroop/info/about-us) to read more about us.

### Index
1. [Setup (Short)](#setup-short)
2. [Setup (Long)](#setup-long)
3. [TOKEN](#bot-token)
4. [Dependencies](#dependencies)
5. [Usage](#usage)
    * [Logging](#logging)
6. [Copyright](#copyright-and-license)


## Setup (Short)
    pip install -r requirements.txt  
* note this installs packages to run the rnn on the cpu by default.  View the tensorflow instrutions under 
[Dependencies](#dependencies) for information on gpu based installation.

Create a file called info.txt in the src directory and place a discord **TOKEN** on the **first line** of the file to 
authorize the bot.  The TOKEN can be found on the bot page of https://discordapp.com/developers/applications/ of one of your applications.

Second, place 
a **path to the model** you would like to generate text with on the **second line** of your info.txt file.
Models can be created with https://github.com/ABlueTortoise30/news_troop_text_generator.

Finally, place the word **"none"** on the **third line** to disable logging.


## Setup (Long)
#### Bot Token
Discord needs to know, that the program running on your computer is the one, that is your bot, so it uses a TOKEN 
to authenticate your bot.  The TOKEN must be kept secret, so I have designed the bot to read it from a separate text 
file.

I did not use a cryptographically secure type of file or any good stuff, so don't guard nukes 
with your discord bot, and use this code at your own risk.

#### Creating a  TOKEN
You must create an "application" through discord's api to create a token.  You can create an application at 
https://discordapp.com/developers/applications/.  There are countless online guides with better writing, than 
I can produce for further assistance, in making a discord application.

#### What to do with your TOKEN
Once you have your TOKEN, place in on the first line of a text file named info.txt in the same directory as 
NEWSTROOP_discord_bot.py, and the bot should be able to automatically read it and appear as a user in your discord 
server.

#### Logging Activity
There are six settings for logging:  requests, curses, articles, headlines, all, or none.  They all write their outputs
 to log.txt.  To enable logging simply write one of five of the latter terms to the third line of info.txt.
* requests - logs whether a headline or article is requested
* curses - logs all instances of use of *cursed words*
* articles - logs if an article is requested
* headlines - logs if a headline is requested
* all - logs both requests and curses
* none - logs nothing; does not create a log file


### Dependencies
https://www.tensorflow.org/ is the heart of the neural network.  Tensorflow 2 is required and version 1.15.0 is no 
longer used.

Install with:

    pip install tensorflow

https://github.com/Rapptz/discord.py is the official python api module for interacting with discord.

Install with:

    pip install discord.py

https://github.com/minimaxir/textgenrnn provides a framework to operate the text generating recurrent neural network. 
Follow minimaxir's 
documentation on how to install his module.


https://github.com/ABlueTortoise30/news_troop_text_generator is another garbled mess of cod3fied nonsense, that ***I*** have 
produced, and it can be used to create models for the NEWSTROOP discord bot to generate text.  Follow the 
[documentation](https://github.com/ABlueTortoise30/news_troop_text_generator/blob/master/README.md) 
to create a model.  Place the absolute path to the file of the model in the second line of your info.txt file.


## Usage
!news, !newstroop, !NEWSTROOP, and !headline will all cause the bot to return a short headline of an article.  
!more, !newstrop, !NEWSTROP, and !article will all provide a full length article of pure text.
!article_long will provide an even longer article

!help will list all commands
!help news will show information on the !news command

All commands can accept words to specify the variety of news requested.  
Example:

    !news Donald Trump
returns

    Donald Trumphing are said they were the election
    

### Logging
There are six settings for logging:  requests, curses, articles, headlines, all, or none.  They all write their outputs
 to log.txt.  To enable logging simply write one of five of the latter terms to the third line of info.txt.
* requests - logs whether a headline or article is requested
* curses - logs all instances of use of *cursed words*
* articles - logs if an article is requested
* headlines - logs if a headline is requested
* all - logs both requests and curses
* none - logs nothing; does not create a log file


## Copyright and License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">NEWS TROOP Discord Notification Bot</span> 
by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/TommyTorty10" property="cc:attributionName" rel="cc:attributionURL">TommyTorty10</a> is licensed under a 
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/minimaxir/textgenrnn" rel="dct:source">https://github.com/minimaxir/textgenrnn</a>.
