#  Copyright (c) 2021. v1.2 under a creative commons license specified in the LICENSE.md file included in the repository.

from discord.ext.commands import Bot
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from textgenrnn.textgenrnn import textgenrnn
from math import floor

try:
    with open('info.txt', 'r') as f:
        TOKEN = f.readline().strip()
        path = f.readline().strip()
        log = f.readline().strip()
        print(path)
except FileExistsError:
    print('read the README and create a info.txt file with your token in the first line and the absolute path to '
          'your model in the second')

BOT_PREFIX = ("?", "!")
client = Bot(command_prefix=BOT_PREFIX, description="NEWSTROOP's official discord news notifier")


def text_gen(request, prompt=False):
    if request == 'headline':
        length = 50
    elif request == 'article':
        length = 500
    else:
        length = 1000

    textgen = textgenrnn(path)

    if prompt:
        prefix = ' '
        prefix = prefix.join(prompt, )
        print(prefix)
        text = textgen.generate(max_gen_length=length, return_as_list=True, prefix=prefix)[0]
    else:
        text = textgen.generate(max_gen_length=length, return_as_list=True)[0]
    return text


@client.command(name='headline',
                description="Official notification of NEWSTROOP headlines",
                brief="Answers from the beyond.",
                aliases=['news', 'newstroop', 'NEWSTROOP', 'bruh', 'BRO'],
                pass_context=True)
async def headline(ctx, *args):
    if log == 'requests' or log == 'all' or log == 'headlines':
        with open('log.txt', 'a+') as f:
            f.write(str(ctx.message.author)+'\n')
            f.write('headline' + '\n')
            if args:
                f.write(str(args) + '\n')
            else:
                f.write('None'+'\n')
            f.write('\n\n')

    text = text_gen('headline', args)
    await ctx.send(text)


@client.command(name='article',
                description="Official notification of NEWSTROOP articles",
                brief="Answers from the beyond.",
                aliases=['more', 'newstrop', 'NEWSTROP', 'bro', 'BRUH', 'situnario'],
                pass_context=True)
async def article(ctx, *args):
    if log == 'requests' or log == 'all' or log == 'articles':
        with open('log.txt', 'a+') as f:
            f.write(str(ctx.message.author)+'\n')
            f.write('article' + '\n')
            if args:
                f.write(str(args) + '\n')
            else:
                f.write('None'+'\n')
            f.write('\n\n')

    text = text_gen('article', args)
    await ctx.send(text)


@client.command(name='article_long',
                description="Official notification of NEWSTROOP articles",
                brief="Answers from the beyond.",
                aliases=['long', 'long_article', 'yowie'],
                pass_context=True)
async def article_long(ctx, *args):
    if log == 'requests' or log == 'all' or log == 'articles':
        with open('log.txt', 'a+') as f:
            f.write(str(ctx.message.author)+'\n')
            f.write('long article' + '\n')
            if args:
                f.write(str(args) + '\n')
            else:
                f.write('None'+'\n')
            f.write('\n\n')

    text = text_gen('long_article', args)
    await ctx.send(text)


@client.event
async def on_message(message):
    curse_words = ['vore', 'child porn', 'SAO']
    if message.author == client.user:
        return

    for word in curse_words:
        if word in message.content:
            if log == 'curses' or log == 'all':
                with open('log.txt', 'a+') as f:
                    f.write(str(message.author)+'\n')
                    f.write('cursed word'+'\n')
                    f.write(message.content+'\n')
                    f.write('\n\n')

            text = 'CURSED WORD SUMMONING INITATED\n\n' + text_gen('article')
            await message.channel.send(text)

    await client.process_commands(message)  # needed to escape on_message


@client.command(name='EverytimeISeeAShirtlessManIHaveToRemindMyselfImNotGay',
                description="use + to separate arguments",
                brief="just a reminder",
                aliases=['philosopher', 'callout'],
                pass_context=True)
async def EverytimeISeeAShirtlessManIHaveToRemindMyselfImNotGay(ctx, *args):
    mush = ''
    for arg in args:
        mush = mush+arg
    real_args = mush.split('+')

    for arg in real_args:
        mlen = len(arg)+2
        message = (' @'+arg)*floor(2000/mlen)
        await ctx.send(message)
        print(arg+' atted')
        if log == 'all':
            with open('log.txt', 'a+') as f:
                f.write(str(ctx.message.author) + '\n')
                f.write('cursed word' + '\n')
                f.write(arg + '\n')
                f.write('\n\n')


client.run(TOKEN)
